package com.vincent.filepicker;

import android.app.Activity;
import android.content.Intent;

import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;

import java.util.ArrayList;

import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.bridge.UniJSCallback;
import io.dcloud.feature.uniapp.common.UniModule;

public class UniPlugin extends UniModule {
    String TAG = "FilePickerModule";
    public UniJSCallback mCallback = null;

    @UniJSMethod(uiThread = true)
    public void openFilePicker(String[] mSuffix, int max, UniJSCallback callback) {
        this.mCallback = callback;
        if ((this.mUniSDKInstance != null) && ((this.mUniSDKInstance.getContext() instanceof Activity))) {
            Activity activity = (Activity) this.mUniSDKInstance.getContext();
            NormalFilePickActivity.start(activity, max, mSuffix, 100);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            ArrayList<NormalFile> normalFiles = data.getParcelableArrayListExtra("ResultPickFILE");
            this.mCallback.invoke(normalFiles);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
